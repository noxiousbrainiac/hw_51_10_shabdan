import './newNumber.css';
import './bootstrap.min.css';

const NewNumber = number => {
    return (
        <div className="card-body d-flex justify-content-evenly p-3">
            {number.children}
        </div>
    );
};

export default NewNumber;