import NewNumber from "./newNumber/newNumber";
import {Component} from "react";

class App extends Component {
  constructor() {
    super();

    this.state = {
      numbers: []
    }
  }

  changeNumber = () => {
    const Array = [];

    while (true){
      if (Array.length === 5){
        break;
      }

      const random = Math.floor(Math.random() * 36) + 1;

      if (Array.includes(random) === true) {
        continue;
      } else {
        Array.push(random);
      }

      Array.sort((a, b) => a > b ? 1: -1);
    }

    this.setState({
      numbers: Array
    })
  }

  render() {
    return (
        <div className="container">
          <div className="newNumber card p-4 m-5">
            <button className="btn btn-primary" onClick={this.changeNumber}>New number</button>
            <NewNumber>
              {this.state.numbers.map((number, index) => {
                return (
                    <p className="number" key={index}>{number}</p>
                )
              })}
            </NewNumber>
          </div>
        </div>
    )
  }
}

export default App;
